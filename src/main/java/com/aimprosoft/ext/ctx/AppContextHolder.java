package com.aimprosoft.ext.ctx;

import com.aimprosoft.ext.ctx.exception.ContextException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Holder for application contexts.
 *
 * @author Vitaliy Koshelenko
 *
 */
public class AppContextHolder {

    /**
     * Map for holding application contexts:
     *  K - context name
     *  V - applicationContext
     */
    private static Map<String, Object> APP_CTX_MAP = new HashMap<>();

    /**
     * Registers application context
     *
     * @param contextName - context name
     * @param context - org.springframework.context.ApplicationContext instance
     *
     * @throws ContextException - when context is null
     */
    public static void registerContext(String contextName, Object context) throws ContextException {
        if (context == null) {
            _log.error("Context (contextName='" + contextName + "') has not been registered, because it's null.");
            throw new ContextException("Context is null for contextName='" + contextName + "'.");
        }
        APP_CTX_MAP.put(contextName, context);
        _log.info("Context (contextName='" + contextName + "') has been registered successfully.");
    }

    /**
     * Removes application context from registry
     *
     * @param contextName - context name
     */
    public static void destroyContext(String contextName) {
        APP_CTX_MAP.remove(contextName);
        _log.info("Context (contextName='" + contextName + "') has been destroyed successfully.");
    }

    /**
     * Returns ApplicationContext instance for specified context name
     *
     * @param contextName - context name
     * @return - ApplicationContext instance for specified context
     *
     * @throws ContextException - when no such context
     */
    public static Object getContext(String contextName) throws ContextException {
        if (!APP_CTX_MAP.containsKey(contextName)) {
            throw new ContextException("Context has not been set for contextName='" + contextName + "'.");
        }
        return APP_CTX_MAP.get(contextName);
    }

    private static Log _log = LogFactoryUtil.getLog(AppContextHolder.class);
}

