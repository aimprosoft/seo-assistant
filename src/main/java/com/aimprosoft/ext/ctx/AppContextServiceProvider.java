package com.aimprosoft.ext.ctx;

import com.aimprosoft.ext.ctx.exception.ContextException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.lang.reflect.Method;

/**
 * Service provider provides service object instance for specified beanName and context name
 *
 * @author Vitaliy Koshelenko
 *
 */
public class AppContextServiceProvider {

    /**
     * Provides service object instance for specified beanName and context name
     *
     * @param contextName - context name
     * @param serviceBeanName - bean name
     *
     * @return - service object
     *
     * @throws Exception - ContextException, when context is null for specified contextName
     */
    public static Object provideService(String contextName, String serviceBeanName) throws Exception {

        //Application Context
        Object context = AppContextHolder.getContext(contextName);
        if (context == null) {
            _log.error("Context has not been set (contextName='" + contextName + "') ");
            throw new ContextException("Context has not been set (contextName='" + contextName + "') ");
        }

        //Service Bean
        Method getBeanMethod = context.getClass().getMethod("getBean", new Class[]{String.class});
        Object serviceObject = getBeanMethod.invoke(context, serviceBeanName);

        _log.info("Service for beanName='" + serviceBeanName + "' and contextName = '" + contextName + "' provided.");

        return serviceObject;
    }

    private static Log _log = LogFactoryUtil.getLog(AppContextServiceProvider.class);
}
