package com.aimprosoft.ext.ctx.exception;

/**
 * Custom exception for context loading issues
 *
 * @author Vitaliy Koshelenko
 */
public class ContextException extends Exception {

    public ContextException() {
        super();
    }

    public ContextException(String message) {
        super(message);
    }

    public ContextException(String message, Throwable cause) {
        super(message, cause);
    }

    public ContextException(Throwable cause) {
        super(cause);
    }
}
