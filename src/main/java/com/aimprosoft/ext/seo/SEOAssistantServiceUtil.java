package com.aimprosoft.ext.seo;

import com.liferay.portal.kernel.util.StringPool;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Locale;

public class SEOAssistantServiceUtil {

    private static final String CONTEXT_NAME = "seo-assistant";
    private static final String BEAN_NAME = "SEOAssistantSharedServiceImpl";


    private static final String SERVICE_PROVIDER_CLASS = "com.aimprosoft.ext.ctx.AppContextServiceProvider";
    private static final String PROVIDE_SERVICE_METHOD = "provideService";

    //com.aimprosoft.common.service.shared.impl.SEOAssistantSharedServiceImpl
    private static  Object SERVICE;


    public static String getUrlTitle(HttpServletRequest request) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlTitle", HttpServletRequest.class);
            return method.invoke(SERVICE, request).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static String getUrlTitle(String url) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlTitle", String.class);
            return method.invoke(SERVICE, url).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static String getUrlTitle(String url, Locale locale) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlTitle", String.class, Locale.class);
            return method.invoke(SERVICE, url, locale).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static String getUrlHeader(HttpServletRequest request) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlHeader", HttpServletRequest.class);
            return method.invoke(SERVICE, request).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static String getUrlHeader(String url) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlHeader", String.class);
            return method.invoke(SERVICE, url).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static String getUrlHeader(String url, Locale locale) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlHeader", String.class, Locale.class);
            return method.invoke(SERVICE, url, locale).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }


    public static String getUrlDescription(HttpServletRequest request) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlDescription", HttpServletRequest.class);
            return method.invoke(SERVICE, request).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static String getUrlDescription(String url) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlDescription", String.class);
            return method.invoke(SERVICE, url).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static String getUrlDescription(String url, Locale locale) {
        try {
            Method method = getService().getClass().getDeclaredMethod("getUrlDescription", String.class, Locale.class);
            return method.invoke(SERVICE, url, locale).toString();
        } catch (Exception e) {
            return StringPool.BLANK;
        }
    }

    public static Object getService() throws Exception {
        if (SERVICE == null) {
            Class<?> serviceProviderClass = Class.forName(SERVICE_PROVIDER_CLASS);
            Method provideServiceMethod = serviceProviderClass.getDeclaredMethod(PROVIDE_SERVICE_METHOD, String.class, String.class);
            SERVICE = provideServiceMethod.invoke(null, CONTEXT_NAME, BEAN_NAME);
        }
        return SERVICE;
    }
}
