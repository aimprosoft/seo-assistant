package com.aimprosoft.configuration.portlet;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Annotation configuration for SEOAssistantPortlet
 *
 * @author Vitaliy Koshelenko
 */
@Configuration
@ComponentScan(basePackages = {
        "com.aimprosoft.web.portlet.controller"
})
public class SEOAssistantPortlet {
}