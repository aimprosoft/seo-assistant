package com.aimprosoft.configuration.common;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

/**
 * Common Spring configuration
 *
 * @author Vitaliy Koshelenko
 */
@Configuration
@ComponentScan(basePackages = {
        "com.aimprosoft.common.config",
        "com.aimprosoft.web.filter",
        "com.aimprosoft.common.service.*"
})
@Log4j2
@PropertySource({
        "classpath:application.properties"
})
@EnableTransactionManagement
public class CommonConfig {

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor(){
        return new LocaleChangeInterceptor();
    }

}