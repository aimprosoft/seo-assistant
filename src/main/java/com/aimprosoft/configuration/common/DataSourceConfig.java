package com.aimprosoft.configuration.common;

import com.liferay.portal.kernel.util.InfrastructureUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Database Spring configuration
 *
 * @author Vitaliy Koshelenko
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.aimprosoft.common.persistence"})
public class DataSourceConfig {

    @Bean
    public DataSource dataSource() {
        return InfrastructureUtil.getDataSource();
    }

    @Bean
    public JpaTransactionManager transactionManager() throws IOException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws IOException {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan(
                "com.aimprosoft.common.model.entity"
        );

        entityManagerFactoryBean.setJpaProperties(jpaProperties());
        entityManagerFactoryBean.setDataSource(dataSource());

        return entityManagerFactoryBean;
    }

    @Bean
    public Properties jpaProperties() throws IOException {
        Properties jpaProperties = new Properties();

        try (InputStream resourceAsStream = getClass().getResourceAsStream("/hibernate.properties")) {
            jpaProperties.load(resourceAsStream);
        }
        return jpaProperties;
    }
}
