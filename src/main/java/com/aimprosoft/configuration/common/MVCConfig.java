package com.aimprosoft.configuration.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * MVC Spring configuration
 *
 * @author Vitaliy Koshelenko
 */
@Configuration
@EnableWebMvc
public class MVCConfig {

    @Bean
    public ViewResolver viewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/html/jsp/portlet/seo-assistant");
        resolver.setSuffix(".jsp");
        resolver.setExposeContextBeansAsAttributes(true);
        resolver.setExposedContextBeanNames("applicationProps", "msgs");
        resolver.setContentType("text/html;charset=UTF-8");
        return resolver;
    }

}
