package com.aimprosoft.configuration;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.web.portlet.context.AbstractRefreshablePortletApplicationContext;

import java.io.IOException;

/**
 * Annotation-based {@link org.springframework.web.context.WebApplicationContext}
 * implementation which takes its configuration from annotated configuration classes
 *
 * {@see https://web.liferay.com/community/forums/-/message_boards/message/31519500}
 * Provide the java configuration for portlets.
 *
 * @author Vitaliy Koshelenko
 */
public class AnnotationPortletApplicationContext extends AbstractRefreshablePortletApplicationContext {

    @Override
    protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) throws BeansException, IOException {
        AnnotationConfigUtils.registerAnnotationConfigProcessors(beanFactory);
        loadConfigBeanDefinitions(beanFactory);
    }

    protected void loadConfigBeanDefinitions(DefaultListableBeanFactory beanFactory) {
        for (String configurationClassName : getConfigLocations()) {
            BeanDefinition configBeanDefinition = new GenericBeanDefinition();
            configBeanDefinition.setBeanClassName(configurationClassName);
            loadConfigBeanDefinition(configBeanDefinition, beanFactory);
        }
    }

    protected void loadConfigBeanDefinition(BeanDefinition beanDefinition, DefaultListableBeanFactory beanFactory) {
        beanFactory.registerBeanDefinition(beanDefinition.getBeanClassName(), beanDefinition);
    }

}

