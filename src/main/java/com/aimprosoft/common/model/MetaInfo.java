package com.aimprosoft.common.model;

public interface MetaInfo {

    Integer getId();

    void setId(Integer id);

    String getUrl();

    void setUrl(String url);

    String getTitle();

    void setTitle(String title);

    String getHeader();

    void setHeader(String header);

    String getDescription();

    void setDescription(String description);
}
