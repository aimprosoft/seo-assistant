package com.aimprosoft.common.model.entity;

import com.aimprosoft.common.model.MetaInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/* Lombok Annotations */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"description"})
@ToString(exclude = "translations")
/* Hibernate Annotations */
@Entity
@Table(name = "url_meta_info")
public class MetaInfoEntity implements MetaInfo {

    @Id
    @Column(name = "url_meta_info_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "url", nullable = false, unique = true)
    private String url;

    @Column(name = "title")
    private String title;

    @Column(name = "header")
    private String header;

    @Lob
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "metaInfo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @BatchSize(size=10)
    private Set<MetaInfoTranslationEntity> translations = new HashSet<>();


}