package com.aimprosoft.common.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/* Lombok Annotations */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = "metaInfo")
@ToString(of = "id")
/* Hibernate Annotations */
@Entity
@Table(name = "url_meta_info_translation")
public class MetaInfoTranslationEntity {

    @Id
    @Column(name = "url_meta_info_translation_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "url_meta_info_id")
    private MetaInfoEntity metaInfo;

    @Column(name = "locale")
    private String locale;

    @Column(name = "title")
    private String title;

    @Column(name = "header")
    private String header;

    @Lob
    @Column(name = "description")
    private String description;

}
