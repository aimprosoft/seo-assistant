package com.aimprosoft.common.model.util;

public interface SEOAssistantConstants {

    String ID = "metaInfoId";

    String URL = "metaInfoUrl";

    String TITLE = "title";

    String HEADER = "header";

    String DESCRIPTION = "description";

}