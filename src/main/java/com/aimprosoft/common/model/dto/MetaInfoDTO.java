package com.aimprosoft.common.model.dto;

import com.aimprosoft.common.model.MetaInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetaInfoDTO implements MetaInfo {

    private Integer id;
    private String url;
    private String header;
    private String title;
    private String description;

}