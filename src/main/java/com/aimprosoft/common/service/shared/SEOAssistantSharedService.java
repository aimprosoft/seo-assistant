package com.aimprosoft.common.service.shared;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public interface SEOAssistantSharedService {

    String getUrlTitle(HttpServletRequest request);

    String getUrlTitle(String url);

    String getUrlTitle(String url, Locale locale);


    String getUrlHeader(HttpServletRequest request);

    String getUrlHeader(String url);

    String getUrlHeader(String url, Locale locale);


    String getUrlDescription(HttpServletRequest request);

    String getUrlDescription(String url);

    String getUrlDescription(String url, Locale locale);
}
