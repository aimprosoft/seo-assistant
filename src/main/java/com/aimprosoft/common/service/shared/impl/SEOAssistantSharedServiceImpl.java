package com.aimprosoft.common.service.shared.impl;

import com.aimprosoft.common.model.dto.MetaInfoDTO;
import com.aimprosoft.common.service.db.URLMetaInfoService;
import com.aimprosoft.common.service.shared.SEOAssistantSharedService;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Objects;

@Service
public class SEOAssistantSharedServiceImpl implements SEOAssistantSharedService {

    private static final String ORIGINAL_URL = "originalUrl";

    private URLMetaInfoService urlMetaInfoService;

    @Autowired
    public SEOAssistantSharedServiceImpl(URLMetaInfoService urlMetaInfoService){
        this.urlMetaInfoService = Objects.requireNonNull(urlMetaInfoService);
    }

    @Override
    public String getUrlTitle(HttpServletRequest request) {
        String urlTitle = StringPool.BLANK;
        if (request.getAttribute(ORIGINAL_URL) != null) {
            urlTitle = getUrlTitle(String.valueOf(request.getAttribute(ORIGINAL_URL)), PortalUtil.getLocale(request));
        }
        if (StringUtils.isBlank(urlTitle)) {
            urlTitle = getUrlTitle(PortalUtil.getCurrentURL(request), PortalUtil.getLocale(request));
        }
        return urlTitle;
    }

    @Override
    public String getUrlTitle(String url) {
        return getUrlTitle(url, Locale.getDefault());
    }

    @Override
    public String getUrlTitle(String url, Locale locale) {
        MetaInfoDTO urlMetaInfo = urlMetaInfoService.getURLMetaInfo(url, locale);
        return urlMetaInfo != null ? urlMetaInfo.getTitle() : StringPool.BLANK;
    }



    @Override
    public String getUrlHeader(HttpServletRequest request) {
        String urlHeader = StringPool.BLANK;
        if (request.getAttribute(ORIGINAL_URL) != null) {
            urlHeader = getUrlHeader(String.valueOf(request.getAttribute(ORIGINAL_URL)), PortalUtil.getLocale(request));
        }
        if (StringUtils.isBlank(urlHeader)) {
            urlHeader = getUrlHeader(PortalUtil.getCurrentURL(request), PortalUtil.getLocale(request));
        }
        return urlHeader;
    }

    @Override
    public String getUrlHeader(String url) {
        return getUrlHeader(url, Locale.getDefault());
    }

    @Override
    public String getUrlHeader(String url, Locale locale) {
        MetaInfoDTO urlMetaInfo = urlMetaInfoService.getURLMetaInfo(url, locale);
        return urlMetaInfo != null ? urlMetaInfo.getHeader() : StringPool.BLANK;
    }


    @Override
    public String getUrlDescription(HttpServletRequest request) {
        String urlDescription = StringPool.BLANK;
        if (request.getAttribute(ORIGINAL_URL) != null) {
            urlDescription = getUrlDescription(String.valueOf(request.getAttribute(ORIGINAL_URL)), PortalUtil.getLocale(request));
        }
        if (StringUtils.isBlank(urlDescription)) {
            urlDescription = getUrlDescription(PortalUtil.getCurrentURL(request), PortalUtil.getLocale(request));
        }
        return urlDescription;
    }

    @Override
    public String getUrlDescription(String url) {
        return getUrlDescription(url, Locale.getDefault());
    }

    @Override
    public String getUrlDescription(String url, Locale locale) {
        MetaInfoDTO urlMetaInfo = urlMetaInfoService.getURLMetaInfo(url, locale);
        return urlMetaInfo != null ? urlMetaInfo.getDescription() : StringPool.BLANK;
    }
}
