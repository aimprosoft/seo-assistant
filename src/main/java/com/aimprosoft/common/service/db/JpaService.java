package com.aimprosoft.common.service.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for base service with JpaRepository methods
 *
 * @param <E> - Entity class
 * @param <ID> - Entity ID class
 *
 * @see JpaRepository
 *
 */
@Transactional
public interface JpaService<E, ID extends Serializable> {

    List<E> findAll();

    E getById(ID entityId);

    <S extends E> S save(S entity);

    void delete(ID entityId);

    void delete(E entity);
}


