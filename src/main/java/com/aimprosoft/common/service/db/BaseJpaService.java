package com.aimprosoft.common.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;

public abstract class BaseJpaService<E, P extends JpaRepository<E, ID>, ID extends Serializable> implements JpaService<E, ID> {

    protected P persistence;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    public void setPersistence(P persistence) {
        this.persistence = persistence;
    }

    @Override
    public List<E> findAll() {
        return persistence.findAll();
    }

    @Override
    public E getById(ID entityId) {
        return persistence.findOne(entityId);
    }

    @Override
    public <S extends E> S save(S entity) {
        return persistence.save(entity);
    }

    @Override
    public void delete(ID entityId) {
        persistence.delete(entityId);
    }

    @Override
    public void delete(E entity) {
        persistence.delete(entity);
    }
}
