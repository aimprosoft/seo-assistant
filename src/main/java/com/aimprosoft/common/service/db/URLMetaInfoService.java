package com.aimprosoft.common.service.db;

import com.aimprosoft.common.model.dto.MetaInfoDTO;
import com.aimprosoft.common.model.MetaInfo;
import com.aimprosoft.common.model.entity.MetaInfoEntity;

import java.util.List;
import java.util.Locale;

public interface URLMetaInfoService extends JpaService<MetaInfoEntity, Integer> {

    MetaInfo getMetaInfoWithTranslations(Integer metaInfoId);

    List<MetaInfoDTO> getMetaInfoList(Locale locale);

    MetaInfoDTO getURLMetaInfo(String url, Locale locale);

    @Override
    <S extends MetaInfoEntity> S save(S entity);

    @Override
    void delete(MetaInfoEntity entity);

    @Override
    void delete(Integer entityId);
}