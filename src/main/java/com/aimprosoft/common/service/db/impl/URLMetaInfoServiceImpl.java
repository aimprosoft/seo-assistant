package com.aimprosoft.common.service.db.impl;

import com.aimprosoft.common.model.MetaInfo;
import com.aimprosoft.common.model.dto.MetaInfoDTO;
import com.aimprosoft.common.model.entity.MetaInfoEntity;
import com.aimprosoft.common.persistence.URLMetaInfoPersistence;
import com.aimprosoft.common.service.db.BaseJpaService;
import com.aimprosoft.common.service.db.URLMetaInfoService;
import com.liferay.portal.kernel.util.LocaleUtil;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class URLMetaInfoServiceImpl
        extends BaseJpaService<MetaInfoEntity, URLMetaInfoPersistence, Integer>
        implements URLMetaInfoService {

    private static final String CACHE_NAME = "seo_cache";

    @Override
    @Cacheable(CACHE_NAME)
    public MetaInfo getMetaInfoWithTranslations(Integer metaInfoId) {
        return persistence.getMetaInfoWithTranslations(metaInfoId);
    }

    @Override
    @Cacheable(CACHE_NAME)
    public List<MetaInfoDTO> getMetaInfoList(Locale locale) {
        String localeName = locale != null ? locale.toString() : LocaleUtil.toLanguageId(Locale.getDefault());
        return persistence.getMetaInfoList(localeName);
    }

    @Override
    @Cacheable(CACHE_NAME)
    public MetaInfoDTO getURLMetaInfo(String url, Locale locale) {
        String localeName = locale != null ? locale.toString() : LocaleUtil.toLanguageId(Locale.getDefault());
        return persistence.getURLMetaInfo(url, localeName);
    }

    @Override
    @CacheEvict(value = CACHE_NAME, allEntries = true)
    public <S extends MetaInfoEntity> S save(S entity) {
        return super.save(entity);
    }

    @Override
    @CacheEvict(value = CACHE_NAME, allEntries = true)
    public void delete(Integer entityId) {
        super.delete(entityId);
    }

    @Override
    @CacheEvict(value = CACHE_NAME, allEntries = true)
    public void delete(MetaInfoEntity entity) {
        super.delete(entity);
    }
}