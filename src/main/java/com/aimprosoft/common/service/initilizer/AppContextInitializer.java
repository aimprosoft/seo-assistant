package com.aimprosoft.common.service.initilizer;

import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.Objects;

@Log4j2
@Service
public class AppContextInitializer implements ApplicationContextAware, InitializingBean, DisposableBean {

    private ApplicationContext applicationContext;

    private static final String CONTEXT_HOLDER_CLASS = "com.aimprosoft.ext.ctx.AppContextHolder";
    private static final String REGISTER_CTX_METHOD = "registerContext";
    private static final String DESTROY_CTX_METHOD = "destroyContext";

    private static final String CONTEXT_NAME = "seo-assistant";

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = Objects.requireNonNull(applicationContext);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            ClassLoader classLoader = PortalClassLoaderUtil.getClassLoader();
            Class<?> appContextHolderClass = classLoader.loadClass(CONTEXT_HOLDER_CLASS);
            Method registerContextMethod = appContextHolderClass.getDeclaredMethod(REGISTER_CTX_METHOD, String.class, Object.class);
            registerContextMethod.invoke(null, CONTEXT_NAME, applicationContext);
        } catch (Exception ignored) {
            //Ignored to not log exceptions when disabled "shared service" mode
        }
    }

    @Override
    public void destroy() throws Exception {
        try {
            ClassLoader classLoader = PortalClassLoaderUtil.getClassLoader();
            Class<?> appContextHolderClass = classLoader.loadClass(CONTEXT_HOLDER_CLASS);
            Method destroyContextMethod = appContextHolderClass.getDeclaredMethod(DESTROY_CTX_METHOD, String.class);
            destroyContextMethod.invoke(null, CONTEXT_NAME);
        } catch (Exception ignored) {
            //Ignored to not log exceptions when disabled "shared service" mode
        }
    }
}
