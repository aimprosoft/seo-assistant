package com.aimprosoft.common.service.facade;

import com.aimprosoft.common.model.MetaInfo;
import com.aimprosoft.common.model.dto.MetaInfoDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface SEOAssistantService {

    List<MetaInfoDTO> getMetaInfoList(Locale locale);

    MetaInfoDTO getMetaInfo(HttpServletRequest request);

    MetaInfo getMetaInfo(Integer metaInfoId);

    MetaInfo updateMetaInfo(Integer metaInfoId, String url, Map<Locale, String> titles, Map<Locale, String> headers, Map<Locale, String> descriptions);

    void deleteMetaInfo(Integer metaInfoId);
}