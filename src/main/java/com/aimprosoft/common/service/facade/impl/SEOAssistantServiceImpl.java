package com.aimprosoft.common.service.facade.impl;

import com.aimprosoft.common.config.AppProps;
import com.aimprosoft.common.model.MetaInfo;
import com.aimprosoft.common.model.dto.MetaInfoDTO;
import com.aimprosoft.common.model.entity.MetaInfoEntity;
import com.aimprosoft.common.model.entity.MetaInfoTranslationEntity;
import com.aimprosoft.common.model.util.SEOAssistantConstants;
import com.aimprosoft.common.service.db.URLMetaInfoService;
import com.aimprosoft.common.service.facade.SEOAssistantService;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class SEOAssistantServiceImpl implements SEOAssistantService {

    private static final String ORIGINAL_URL = "originalUrl";

    private URLMetaInfoService metaInfoService;
    private AppProps appProps;

    @Autowired
    public SEOAssistantServiceImpl(URLMetaInfoService urlMetaInfoService, AppProps appProps) {
        this.metaInfoService = Objects.requireNonNull(urlMetaInfoService);
        this.appProps = Objects.requireNonNull(appProps);
    }

    @Override
    public List<MetaInfoDTO> getMetaInfoList(Locale locale) {
        List<MetaInfoDTO> metaInfoList = metaInfoService.getMetaInfoList(locale);
        if (CollectionUtils.isNotEmpty(metaInfoList)) {
            metaInfoList.forEach(mi -> {
                String description = mi.getDescription();
                String strippedDescription = HtmlUtil.stripHtml(description);
                String shortenDescription = StringUtils.abbreviate(strippedDescription, appProps.getMetaInfoDescriptionMaxLength());
                mi.setDescription(shortenDescription);
            });
        }
        return metaInfoList;
    }

    @Override
    public MetaInfoDTO getMetaInfo(HttpServletRequest request) {
        String url = request.getAttribute(ORIGINAL_URL) != null ? String.valueOf(request.getAttribute(ORIGINAL_URL)) : PortalUtil.getCurrentURL(request);
        Locale locale = PortalUtil.getLocale(request);
        return metaInfoService.getURLMetaInfo(url, locale);
    }

    @Override
    public MetaInfo getMetaInfo(Integer metaInfoId) {
        return (metaInfoId != null && metaInfoId > 0) ?
                convertToDTO((MetaInfoEntity) metaInfoService.getMetaInfoWithTranslations(metaInfoId)) : new MetaInfoDTO();
    }

    @Override
    public MetaInfo updateMetaInfo(Integer metaInfoId, String url, Map<Locale, String> titles, Map<Locale, String> headers,
                                   Map<Locale, String> descriptions) {

        MetaInfoEntity metaInfoEntity = (metaInfoId != null && metaInfoId > 0) ?
                (MetaInfoEntity) metaInfoService.getMetaInfoWithTranslations(metaInfoId) : new MetaInfoEntity();
        metaInfoEntity.setUrl(url);

        Map<Locale, MetaInfoDTO> metaInfoMap = getMetaInfoMap(titles, headers, descriptions);

        populateTranslations(metaInfoEntity, metaInfoMap);

        Locale defaultLocale = LocaleUtil.getDefault();
        String defaultTitle = titles.get(defaultLocale);
        String defaultHeader = headers.get(defaultLocale);
        String defaultDescription = descriptions.get(defaultLocale);

        metaInfoEntity.setTitle(defaultTitle);
        metaInfoEntity.setHeader(defaultHeader);
        metaInfoEntity.setDescription(defaultDescription);

        return metaInfoService.save(metaInfoEntity);
    }


    @Override
    public void deleteMetaInfo(Integer metaInfoId) {
        metaInfoService.delete(metaInfoId);
    }

    private Map<Locale, MetaInfoDTO> getMetaInfoMap(Map<Locale, String> titles, Map<Locale, String> headers, Map<Locale, String> descriptions) {
        Map<Locale,MetaInfoDTO> metaInfoMap = new HashMap<>();
        for (Locale locale : titles.keySet()) {
            String title = titles.get(locale);
            MetaInfoDTO dto = metaInfoMap.containsKey(locale) ? metaInfoMap.get(locale) : new MetaInfoDTO();
            dto.setTitle(title);
            metaInfoMap.put(locale, dto);
        }
        for (Locale locale : headers.keySet()) {
            String header = headers.get(locale);
            MetaInfoDTO dto = metaInfoMap.containsKey(locale) ? metaInfoMap.get(locale) : new MetaInfoDTO();
            dto.setHeader(header);
            metaInfoMap.put(locale, dto);
        }
        for (Locale locale : descriptions.keySet()) {
            String description = descriptions.get(locale);
            MetaInfoDTO dto = metaInfoMap.containsKey(locale) ? metaInfoMap.get(locale) : new MetaInfoDTO();
            dto.setDescription(description);
            metaInfoMap.put(locale, dto);
        }
        return metaInfoMap;
    }

    private void populateTranslations(MetaInfoEntity metaInfoEntity, Map<Locale, MetaInfoDTO> metaInfoMap) {
        Set<MetaInfoTranslationEntity> translations = metaInfoEntity.getTranslations();
        for (Locale locale : metaInfoMap.keySet()) {
            MetaInfoDTO metaInfoDTO = metaInfoMap.get(locale);
            MetaInfoTranslationEntity translation = translations
                    .stream()
                    .filter(t -> locale.equals(LocaleUtil.fromLanguageId(t.getLocale())))
                    .findAny()
                    .orElse(null);
            if (translation == null) {
                translation = new MetaInfoTranslationEntity();
                translation.setLocale(LocaleUtil.toLanguageId(locale));
                translation.setMetaInfo(metaInfoEntity);
                translations.add(translation);
            }
            if (!StringPool.BLANK.equals(metaInfoDTO.getTitle())) {
                translation.setTitle(metaInfoDTO.getTitle());
            }
            if (!StringPool.BLANK.equals(metaInfoDTO.getHeader())) {
                translation.setHeader(metaInfoDTO.getHeader());
            }
            if (!StringPool.BLANK.equals(metaInfoDTO.getDescription())) {
                translation.setDescription(metaInfoDTO.getDescription());
            }
        }
    }

    private MetaInfo convertToDTO(MetaInfoEntity metaInfo) {
        MetaInfo metaInfoDTO = new MetaInfoDTO();

        Integer metaInfoId = metaInfo.getId();
        String url = metaInfo.getUrl();

        String title = metaInfo.getTitle();
        String header = metaInfo.getHeader();
        String description = metaInfo.getDescription();

        Map<Locale, String> titles = new HashMap<>();
        Map<Locale, String> headers = new HashMap<>();
        Map<Locale, String> descriptions = new HashMap<>();

        Locale defaultLocale = LocaleUtil.getDefault();
        titles.put(defaultLocale, title);
        headers.put(defaultLocale, header);
        descriptions.put(defaultLocale, description);

        Set<MetaInfoTranslationEntity> translations = metaInfo.getTranslations();
        if (CollectionUtils.isNotEmpty(translations)) {
            for (MetaInfoTranslationEntity translation : translations) {
                Locale locale = LocaleUtil.fromLanguageId(translation.getLocale());
                String translationTitle = translation.getTitle();
                String translationHeader = translation.getHeader();
                String translationDescription = translation.getDescription();
                if (StringUtils.isNoneBlank(translationTitle)) {
                    titles.put(locale, translationTitle);
                }
                if (StringUtils.isNoneBlank(translationHeader)) {
                    headers.put(locale, translationHeader);
                }
                if (StringUtils.isNoneBlank(translationDescription)) {
                    descriptions.put(locale, translationDescription);
                }
            }
        }

        String titleXml = LocalizationUtil.updateLocalization(titles, StringPool.BLANK, SEOAssistantConstants.TITLE, LocaleUtil.toLanguageId(LocaleUtil.getDefault()));
        String headerXml = LocalizationUtil.updateLocalization(headers, StringPool.BLANK, SEOAssistantConstants.HEADER, LocaleUtil.toLanguageId(LocaleUtil.getDefault()));
        String descriptionXml = LocalizationUtil.updateLocalization(descriptions, StringPool.BLANK, SEOAssistantConstants.DESCRIPTION, LocaleUtil.toLanguageId(LocaleUtil.getDefault()));

        metaInfoDTO.setId(metaInfoId);
        metaInfoDTO.setUrl(url);
        metaInfoDTO.setTitle(titleXml);
        metaInfoDTO.setHeader(headerXml);
        metaInfoDTO.setDescription(descriptionXml);

        return metaInfoDTO;
    }
}
