package com.aimprosoft.common.persistence;

import com.aimprosoft.common.model.dto.MetaInfoDTO;
import com.aimprosoft.common.model.MetaInfo;
import com.aimprosoft.common.model.entity.MetaInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface URLMetaInfoPersistence extends JpaRepository<MetaInfoEntity, Integer> {

    /**
     * Fetches MetaInfoEntity with translations for specified ID
     *
     * @param metaInfoId - ID of MetaInfoEntity
     * @return - MetaInfoEntity object
     */
    @Query(value = "select distinct umi from MetaInfoEntity umi join fetch umi.translations where umi.id = ?1")
    MetaInfo getMetaInfoWithTranslations(Integer metaInfoId);

    /**
     * Fetches localized META information for URL
     *
     * @param locale - Locale
     * @return - List of matched MetaInfoDTO object
     */
    @Query(value = "select new com.aimprosoft.common.model.dto.MetaInfoDTO(" +
            " origin.id, " +
            " origin.url, " +
            " COALESCE(translation.header, origin.header), " +
            " COALESCE(translation.title, origin.title), " +
            " COALESCE(translation.description, origin.description) " +
            ") " +
            "from MetaInfoEntity origin " +
            "left join MetaInfoTranslationEntity translation on origin.id=translation.metaInfo.id and translation.locale = ?1 ")
    List<MetaInfoDTO> getMetaInfoList(String locale);

    /**
     * Fetches localized META information for URL
     *
     * @param url - Friendly URL
     * @param locale - Locale
     * @return - List of matched MetaInfoDTO object
     */
    @Query(value = "select new com.aimprosoft.common.model.dto.MetaInfoDTO(" +
            " origin.id, " +
            " origin.url, " +
            " COALESCE(translation.header, origin.header), " +
            " COALESCE(translation.title, origin.title), " +
            " COALESCE(translation.description, origin.description) " +
            ") " +
            "from MetaInfoEntity origin " +
            "left join MetaInfoTranslationEntity translation on origin.id=translation.metaInfo.id and translation.locale = ?2 " +
            "where origin.url = ?1 ")
    MetaInfoDTO getURLMetaInfo(String url, String locale);

}