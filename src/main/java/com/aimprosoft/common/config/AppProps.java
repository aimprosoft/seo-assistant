package com.aimprosoft.common.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * Service for accessing properties (defined in application.properties)
 *
 * @author Vitaliy Koshelenko
 */
@Data
@Service
public class AppProps {

    @Value("${meta.info.description.max.length}")
    private int metaInfoDescriptionMaxLength;

}
