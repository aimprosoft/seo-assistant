package com.aimprosoft.web.portlet.controller;

import com.aimprosoft.common.model.MetaInfo;
import com.aimprosoft.common.model.dto.MetaInfoDTO;
import com.aimprosoft.common.model.util.SEOAssistantConstants;
import com.aimprosoft.common.service.facade.SEOAssistantService;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.util.PortalUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

@Log4j2
@Controller
@RequestMapping("VIEW")
public class SEOAssistantController {

    // Services
    private SEOAssistantService seoAssistantService;

    // JSP views
    private static final String VIEW_PAGE = "/view/view";
    private static final String EDIT_PAGE = "/edit/edit";
    private static final String ERROR_PAGE = "/error";

    // Parameter Constants
    private static final String META_INFO = "metaInfo";
    private static final String META_INFOS = "metaInfos";

    @Autowired
    public SEOAssistantController(SEOAssistantService seoAssistantService){
        this.seoAssistantService = Objects.requireNonNull(seoAssistantService);
    }

    @RenderMapping
    public String defaultView(RenderRequest renderRequest, RenderResponse renderResponse, Model model) {
        Locale locale = PortalUtil.getLocale(renderRequest);
        List<MetaInfoDTO> metaInfoDTOList = seoAssistantService.getMetaInfoList(locale);
        model.addAttribute(META_INFOS, metaInfoDTOList);
        return VIEW_PAGE;
    }

    @RenderMapping(params = "action=editMetaInfoView")
    public String editMetaInfoView(RenderRequest renderRequest, RenderResponse renderResponse,
                                   @RequestParam(value = SEOAssistantConstants.ID, required = false) Integer metaInfoId) {
        MetaInfo metaInfo = seoAssistantService.getMetaInfo(metaInfoId);
        renderRequest.setAttribute(META_INFO, metaInfo);
        return EDIT_PAGE;
    }

    @ActionMapping(params = "action=editMetaInfo")
    public void editMetaInfo(ActionRequest actionRequest, ActionResponse actionResponse,
                             @RequestParam(value = SEOAssistantConstants.ID, required = false) Integer metaInfoId,
                             @RequestParam(value = SEOAssistantConstants.URL, required = false) String url) {
        Map<Locale, String> titles = LocalizationUtil.getLocalizationMap(actionRequest, SEOAssistantConstants.TITLE);
        Map<Locale, String> headers = LocalizationUtil.getLocalizationMap(actionRequest, SEOAssistantConstants.HEADER);
        Map<Locale, String> descriptions = LocalizationUtil.getLocalizationMap(actionRequest, SEOAssistantConstants.DESCRIPTION);
        seoAssistantService.updateMetaInfo(metaInfoId, url, titles, headers, descriptions);
    }

    @ActionMapping(params = "action=deleteMetaInfo")
    public void deleteMetaInfo(ActionRequest actionRequest, ActionResponse actionResponse,
                               @RequestParam(value = SEOAssistantConstants.ID, required = false) Integer metaInfoId) {
        seoAssistantService.deleteMetaInfo(metaInfoId);
    }

    @ExceptionHandler(Exception.class)
    public String defaultExceptionHandler(Exception e) {
        log.error(e,e);
        return ERROR_PAGE;
    }
}