package com.aimprosoft.web.filter;

import com.aimprosoft.common.model.dto.MetaInfoDTO;
import com.aimprosoft.common.service.facade.SEOAssistantService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

@Log4j2
@Component
public class SEOAssistantFilter implements Filter {

    private SEOAssistantService seoAssistantService;

    @Autowired
    public SEOAssistantFilter(SEOAssistantService seoAssistantService) {
        this.seoAssistantService = Objects.requireNonNull(seoAssistantService);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("Initializing SEO Assistant Filter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        try {
            servletRequest.setCharacterEncoding("utf-8");
            HttpServletRequest request = (HttpServletRequest)servletRequest;

            MetaInfoDTO metaInfo = seoAssistantService.getMetaInfo(request);
            if (metaInfo != null) {
                request.setAttribute("seoTitle", metaInfo.getTitle());
                request.setAttribute("seoHeader", metaInfo.getHeader());
                request.setAttribute("seoDescription", metaInfo.getDescription());
            }

        } catch (Exception e) {
            log.error("Exception in SEOAssistantFilter: " + e.getMessage());
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        log.info("Destroying SEO Assistant Filter");
    }
}
