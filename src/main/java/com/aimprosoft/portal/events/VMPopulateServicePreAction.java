package com.aimprosoft.portal.events;

import com.aimprosoft.ext.seo.SEOAssistantServiceUtil;
import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Injects SEOAssistantSharedServiceImpl instance to Velocity Context
 *
 * {@see https://web.liferay.com/web/v.koshelenko/blog/-/blogs/adding-custom-classes-to-theme-velocity-context}
 *
 * @author Vitaliy Koshelenko
 */
public class VMPopulateServicePreAction extends Action {

    @Override
    @SuppressWarnings("unchecked")
    public void run(HttpServletRequest request, HttpServletResponse response) {
        Map vmVariablesMap = (Map) request.getAttribute(WebKeys.VM_VARIABLES);
        if (vmVariablesMap == null) {
            vmVariablesMap = new HashMap<String, Object>();
        }
        try {
            vmVariablesMap.put("seoService", SEOAssistantServiceUtil.getService());
        } catch (Exception e) {
            _log.error(e, e);
        }
        request.setAttribute(WebKeys.VM_VARIABLES, vmVariablesMap);
    }

    private static Log _log = LogFactoryUtil.getLog(VMPopulateServicePreAction.class);
}