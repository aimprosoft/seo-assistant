<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@include file="../init.jsp"%>

<c:set var="defaultLanguageId" value="<%= PortalUtil.getLocale(request) %>" />
<c:set var="languageId" value="<%= PortalUtil.getLocale(request) %>" />
<c:set var="availableLocales" value="<%=  LanguageUtil.getAvailableLocales() %>" />