<%@include file="init-common.jsp" %>
<div id="seo-wrapper">
    <h1><liferay-ui:message key="meta.info.edit.header" /></h1>
    <form name="edit-meta-info-form" id="edit-meta-info-form" method="post" action="${editMetaInfoURL}">
        <input type="hidden" name="<portlet:namespace/>${metaInfoId}" value="${metaInfo.id}" />
        <table>
            <tr>
                <td>#</td>
                <td>${metaInfo.id}</td>
            </tr>
            <tr>
                <td>
                    <label for="mi-url"><liferay-ui:message key="meta.info.url" />:</label>
                </td>
                <td>
                    <input type="text" id="mi-url" name="<portlet:namespace/>${metaInfoUrl}" value="${metaInfo.url}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="mi-title"><liferay-ui:message key="meta.info.title" />:</label>
                </td>
                <td>
                    <liferay-ui:input-localized id="mi-title" name="title" xml="${metaInfo.title}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="mi-url"><liferay-ui:message key="meta.info.header" />:</label>
                </td>
                <td>
                    <liferay-ui:input-localized id="mi-header" name="header" xml="${metaInfo.header}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="mi-description"><liferay-ui:message key="meta.info.description" />:</label>
                </td>
                <td>
                    <liferay-ui:input-localized id="mi-description" name="description" xml="${metaInfo.description}" />
                    <%-- todo: make editor work --%>
                    <%--<liferay-ui:input-localized id="mi-description" name="description" xml="${metaInfo.description}"
                type="editor" availableLocales="${availableLocales}"  languageId="${languageId}" defaultLanguageId="${defaultLanguageId}" />--%>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="${defaultViewURL}">Cancel</a>
                </td>
                <td>
                    <input type="submit" name="Save" />
                </td>
            </tr>
        </table>
    </form>
</div>