<%@include file="init.jsp"%>
<div class="alert alert-error"><liferay-ui:message key="meta.info.error"/></div>
<a class="back-link" href="javascript:history.go(-1);"><liferay-ui:message key="back" /></a>