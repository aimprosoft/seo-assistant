<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %>
<%@ page import="com.aimprosoft.common.model.util.SEOAssistantConstants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page pageEncoding="UTF-8" %>

<liferay-theme:defineObjects/>
<portlet:defineObjects/>

<c:set var="path" value="${pageContext.servletContext.contextPath}"/>
<c:set var="normal" value="<%=LiferayWindowState.NORMAL.toString()%>"/>
<c:set var="exclusive" value="<%=LiferayWindowState.EXCLUSIVE.toString()%>"/>

<c:set var="metaInfoId" value="<%= SEOAssistantConstants.ID %>"/>
<c:set var="metaInfoUrl" value="<%= SEOAssistantConstants.URL %>"/>

<portlet:renderURL var="defaultViewURL" />

<portlet:renderURL var="editMetaInfoViewURL">
    <portlet:param name="action" value="editMetaInfoView" />
</portlet:renderURL>

<portlet:actionURL var="editMetaInfoURL">
    <portlet:param name="action" value="editMetaInfo"/>
</portlet:actionURL>

<portlet:actionURL var="deleteMetaInfoURL">
    <portlet:param name="action" value="deleteMetaInfo"/>
</portlet:actionURL>