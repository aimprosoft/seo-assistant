<%@include file="init-common.jsp" %>
<div id="seo-wrapper">
    <h1><liferay-ui:message key="meta.info.list.header" /></h1>
    <table id="seo-table">
        <thead>
            <tr>
                <th>#</th>
                <th><liferay-ui:message key="meta.info.url" /></th>
                <th><liferay-ui:message key="meta.info.title" /></th>
                <th><liferay-ui:message key="meta.info.header" /></th>
                <th><liferay-ui:message key="meta.info.description" /></th>
                <th> </th>
                <th> </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="metaInfo" items="${metaInfos}">
                <tr>
                    <td>${metaInfo.id}</td>
                    <td>${metaInfo.url}</td>
                    <td>${metaInfo.header}</td>
                    <td>${metaInfo.title}</td>
                    <td>${metaInfo.description}</td>
                    <td>
                        <liferay-ui:icon image="delete" label="<%= true %>" url="${deleteMetaInfoURL}&${metaInfoId}=${metaInfo.id}" />
                    </td>
                    <td>
                        <liferay-ui:icon image="edit" label="<%= true %>" url="${editMetaInfoViewURL}&${metaInfoId}=${metaInfo.id}" />
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <hr/>
    <a href="${editMetaInfoViewURL}">
        <liferay-ui:icon image="add" label="<%= true %>" url="${editMetaInfoViewURL}" />
    </a>
</div>