#SEO Assistant Portlet

##Introduction

SEO Assistant portlet provides tools for site SEO manager to specify URL-specific metadata (like page header, title, description)
for any portal URL (URL to layout, friendly URL, URL with specified parameters, etc.).

Liferay provides functionality for specifying SEO-related data out-of-the box, but only for Liferay layouts.
SEO Assistant solutions is more flexible, as it provides possibility to specify different page setting for different friendly URLs on the same layout 
(like Wiki page, MB Thread, Web Content article, some custom functionality, etc).

##Usage

SEO Assistant provides means for SEO Administrator to specify localized metadata for specified URL.
SEO Assistant does not inject this information to the page, but sets it to request attributes (for better customization).
You may get this information from request attributes in theme, Java/jsp code, like:


request.getAttribute("seoTitle")

request.getAttribute("seoHeader")

request.getAttribute("seoDescription")

##Samples

### SEO-Assistant - metadata management

![Metadata-management](images/01.jpg)

### SEO-Assistant in action

![Metadata-in-action](images/02.jpg)
